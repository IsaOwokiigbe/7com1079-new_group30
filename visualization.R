library(tidyverse)
pdf("visualization.pdf")

crimes_against_women_2001_2014<-read_csv("crimes_against_women_2001-2014.csv")
a<-crimes_against_women_2001_2014$Rape
b<-crimes_against_women_2001_2014$`Kidnapping and Abduction`
c<-crimes_against_women_2001_2014$Year

m<-as.data.frame(aggregate(x=a, by= list(c), FUN = sum))
n<-as.data.frame(aggregate(x=b, by= list(c), FUN = sum))

plot(jitter(m$Group.1,1),m$x, xlab = "Year", ylab = "No. of Crime", main = "Yearly Rate of Crime for Rape")
crime1<-lm(m$x~m$Group.1, data = m)
abline(crime1, col= "red")

plot(jitter(n$Group.1,1),n$x, xlab = "Year", ylab = "No. of Crime", main = "Yearly Rate of Crime for Kidnapping and Abduction")
crime2<-lm(n$x~n$Group.1, data = n)
abline(crime2, col= "red")

u<-data.frame("Aggm"=c(m$x),"Aggn"=c(n$x))
View(u)
plot(jitter(u$Aggn,1),u$Aggn, xlab = "No. of Kidnap cases/Year", ylab = "No. of Rape Cases/Year ", main = "Rape Vs Kidnapping and Abduction")
crime3<-lm(u$Aggm~u$Aggn, data = u)
abline(crime3, col= "red")

rapeMean<-mean(m$x,na.rm = TRUE)
rapeSD<-sd(m$x,na.rm = TRUE)
kidnapMean<-mean(n$x,na.rm = TRUE)
kidnapSD<-sd(n$x,na.rm = TRUE)
bMean<-mean(b,na.rm = TRUE)

dev.off()
